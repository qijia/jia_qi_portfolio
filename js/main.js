(function() {
  var theImages = document.querySelectorAll('.parent-image');
  var submitForm = document.querySelector('#contactForm');

  theImages.forEach(function(element, index) {
    element.addEventListener('click', function() { popLightbox(element.src); }, false);
  });
  
  submitForm.addEventListener('submit', sendMessage, false);
  
  function sendMessage(e){
    e.preventDefault();
    var xhr = new XMLHttpRequest();
    xhr.open("POST", 'mail.php', true);
    xhr.setRequestHeader("Content-type", "application/x-www-form-urlencoded");

    xhr.onreadystatechange = function() {
      if(xhr.readyState == XMLHttpRequest.DONE && xhr.status == 200) {
        // Request finished. Do processing here.  
        let resultDiv = document.querySelector('#sendMessageResult'); 
        if(this.responseText === "1")
        {
          resultDiv.innerHTML = "Message sent. Thank you.";          
        }
        else{
          resultDiv.innerHTML = "Message not sent. One or more fields contain invalid data."; 
        }
        
        // Show the result message for 1.5 seconds and hide it again.
        resultDiv.style.display = "block";
        var millisecondsToWait = 1500;
        setTimeout(function() {resultDiv.style.display = "none";}, millisecondsToWait);            
      }
    }
   
    // Get input data from the form and put it into JSON format.
    var data = "";
    var formInputCount = submitForm.length;
    for (var i = 0; i < formInputCount; ++i) {
      var input = submitForm[i];
      if (input.name && input.name !== "submit") {
      	
      	// Form a query string to be posted to the server, and make sure the input value is URI encoded so that
      	// special characters can be properly handled.
        data = data + input.name + "=" + encodeURIComponent(input.value) + "&"
      }
    }

    // Remove the ending '&'
    data = data.slice(0, -1);
    xhr.send(data); 
  }

  function popLightbox(src) {
    // Ajax query the text fields that need to be displayed on the UI.
    var xmlhttp = new XMLHttpRequest();
        xmlhttp.onreadystatechange = function() {
            if (this.readyState == XMLHttpRequest.DONE && this.status == 200) {
                var data = JSON.parse(this.responseText);
                renderLightbox(data, src);
            }
        };

    // Make a local copy so the original string value is not changed.
    var localSrc = src;
    var url = localSrc.replace("getImage", "getTextFields").replace("&type=summary", "");
    xmlhttp.open("GET", url, true);
    xmlhttp.send();
  }

  function renderLightbox(textData, imgSrc){
    document.body.style.overflow = "hidden";
    let lightbox = document.querySelector('.lightbox');
    let lightboxClose = lightbox.querySelector('.close-lightbox');
    let lightboxImage = document.querySelector('#lightboxImage');
    let lightboxDesc = document.querySelector('#lightboxDesc');
    let lightboxCloseButton = document.querySelector('#lightboxCloseButton');
    let lightboxSiteLink = document.querySelector('#lightboxLink');
    lightbox.style.display = "block";
    lightboxImage.src = imgSrc.replace("summary", "detail");
    lightboxDesc.innerHTML = textData.image_description;
    lightboxCloseButton.innerHTML = textData.image_closelinktext;
    if(textData.image_sitelink){
      lightboxSiteLink.style.display = "block";
      lightboxSiteLink.href = textData.image_sitelink;
      lightboxSiteLink.innerHTML = textData.image_websitelinktext;
    }
    else{
      lightboxSiteLink.style.display = "none";
    }
    lightboxClose.addEventListener('click', closeLightbox, false);
    lightboxCloseButton.addEventListener('click', closeLightbox, false);
  }

  function closeLightbox() {
    let lightbox = document.querySelector('.lightbox');
    lightbox.style.display = "";
    document.body.style.overflow = "";
  }
  
  
  //scrollto
  var about = document.querySelector("#id1");
  var works = document.querySelector("#id2");
  var contact = document.querySelector("#id3");
 
  function scrollToAbout(e) {
		e.preventDefault();
		TweenLite.to(window, 1, {scrollTo:{y:"#aboutMe", offsetY:150}});
	}
	
  function scrollToWorks(e) {
		e.preventDefault();
		TweenLite.to(window, 1, {scrollTo:{y:"#worksImgs", offsetY:70}});
	}
	
  function scrollToContact(e) {
		e.preventDefault();
		TweenLite.to(window, 1, {scrollTo:{y:"#contact", offsetY:150}});
	}

  about.addEventListener("click", scrollToAbout, false);
  works.addEventListener("click", scrollToWorks, false);
  contact.addEventListener("click", scrollToContact, false);
  
  
  //build page
  var bg = document.querySelector("body")
  
  function buildPg() {
		TweenMax.to(bg, 1, {opacity:1});
	}
  
  window.addEventListener("load", buildPg, false);
  
  
  //scroll to show
  var aboutArea = document.querySelector("#about");
  var worksArea = document.querySelector("#works");
  var contactArea = document.querySelector("#contact");
  var screenPOS;
  var text;
  
  function showAbout() {
		screenPOS = window.scrollY;
		text = aboutArea.offsetTop;
			if(screenPOS+350>text) {
			document.querySelector("#about").style.opacity="100";
			window.removeEventListener("scroll", showAbout, false);	
		}
	}	
	
  function showWorks() {
		screenPOS = window.scrollY;
		text = worksArea.offsetTop;
			if(screenPOS+200>text) {
			document.querySelector("#works").style.opacity="100";
			window.removeEventListener("scroll", showWorks, false);	
		}
	}	
	
  function showContact() {
		screenPOS = window.scrollY;
		text = contactArea.offsetTop;
			if(screenPOS+300>text) {
			document.querySelector("#contact").style.opacity="100";
			window.removeEventListener("scroll", showContact, false);	
		}
	}	
  
  window.addEventListener("scroll", showAbout, false);
  window.addEventListener("scroll", showWorks, false);
  window.addEventListener("scroll", showContact, false);
  
  
  
})();
