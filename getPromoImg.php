<?php
  ini_set("display_errors",1);
  error_reporting(E_ALL);
  include("connect.php");

  //Get a single image.
  $id = $_GET['id'];
  $imgQuery = "SELECT * FROM tbl_promoimg WHERE promoimg_id=$id";
  $getImage = mysqli_query($link, $imgQuery);

  // Check the result, only render the image when there is one and only one image record returned.
  if ($getImage->num_rows == 1) {
      // Get the image and render it
      $row = $getImage->fetch_assoc();
      header("Content-type: image/jpg");
      echo $row['promoimg_data'];

  } else {
      // Else, just output the error message.
      echo "Cannot find a matching image. Either no row or more than one row found in DB with the specified id.";
  }

  // Close the connection.
  mysqli_close($link);
?>