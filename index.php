<!doctype html>
<html class="no-js" lang="en" dir="ltr">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="x-ua-compatible" content="ie=edge">
    <meta name="viewport" content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <title>Qi Jia (Jay)</title>
    <link href="https://fonts.googleapis.com/css?family=Roboto:400,700,900" rel="stylesheet">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">
    <link rel="stylesheet" href="css/foundation.css">
    <link rel="stylesheet" href="css/main.css">
    <script src="js/TweenMax.min.js"></script>
	  <script src="js/ScrollToPlugin.min.js"></script>
  </head>


  <body>
    <h1 class="hide">Qi Jia Portfolio</h1>

    <header id="mainHeader">
      <div class="row">
        <div id="menuLogo" class="small-3 medium-1 medium-offset-1 large-1 large-offset-1 columns">
          <img src="images/qijia_logo.svg" alt="qijialogo" id="qijiaLogo">
        </div>
        <div class="title-bar" data-responsive-toggle="main-menu" data-hide-for="medium" data-hide-for="large">
    	    <button class="menu-icon float-right" type="button" data-toggle></button>
        </div>

        <div id="nav" class="small-12 medium-4 medium-offset-5 large-4 large-offset-5 columns">
    	    <h2 class="hide">Main Navigation</h2>
            <div id="main-menu">
	      <ul id="mainNav">
            	  <li><a href="#" id="id1">ABOUT</a></li>
            	  <li><a href="#" id="id2">WORKS</a></li>
            	  <li><a href="#" id="id3">CONTACT</a></li>
              </ul>
            </div>
        </div>
      </div>
    </header>


    <!--promo-->
    <section id="promoBackground">
      <h2 class="hide">Promo picture</h2>
        
        <div id="promo" >
            <div class="row">
                <p id="promoText" class="small-12 medium-12 large-12 medium-offset-1 large-offset-1 columns">Hey, I am Qi Jia. <br>I am a website designer.
                </p>
            </div>
		<div id="promoImg1"><img src="getPromoImg.php?id=1" ></div>
		<div id="promoImg2"><img src="getPromoImg.php?id=3" ></div>
		<div id="promoImg3"><img src="getPromoImg.php?id=4" ></div>
        </div>
    </section>


    <!-- text -->
    <section id="about" class="row">
      <h2 class="hide">About Me</h2>
          <p id="aboutMe" class="small-12 medium-8 large-8 medium-offset-4 large-offset-4 columns">My name is Qi Jia. I am a designer focused on visual communication. I enjoy experience any new things which can bring me some extremely good ideas and inspirations, then I can put them into my design. I am also a music and sport lover, which can make me feel more passion. Currently I am living in London, Ontario.</p>

          <div id="careerTitle" class="small-12 medium-12 large-12 medium-offset-1 large-offset-1 columns">
            <p>Career</p>
            <hr>
          </div>
         

	  <div id="companies">
          <p id="eagle" class="small-9 medium-12 medium-offset-2 large-4 large-offset-2 columns">&nbsp;&nbsp;&nbsp;&nbsp;Eagle co.,Ltd.<br>— 2012 - 2015</p>
          <p id="gaoshi" class="small-12 medium-12 large-6 medium-offset-5 large-offset-5 columns">&nbsp;&nbsp;&nbsp;&nbsp;Gaoshi advertising co.,Ltd.<br>— 2010 - 2012</p>
          <p id="northLand" class="small-9 medium-4 large-4 medium-offset-8 large-offset-8 columns">&nbsp;&nbsp;&nbsp;&nbsp;North land<br>— 2009 - 2010</p>
          <p id="yigouguanjing" class="small-12 medium-10 large-10 medium-offset-2 large-offset-2 columns">&nbsp;&nbsp;&nbsp;&nbsp;Yigouguanjing<br>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;landscape co.,Ltd<br>— 2009 - 2009</p>
         </div>
    </section>


    <!-- works -->
    <div id="worksBackground">
    <section id="works" class="row">
      <h2 class="hide">Works</h2>
      
      <div id="worksTitle" class="small-12 medium-3 large-3 medium-offset-8 large-offset-8 columns">
        <p>Works</p>
        <hr>
      </div>
      <div id="worksImgs">
        <div id="LOTR" class="small-6 medium-4 large-4 columns">
          <img src="getImage.php?id=1&type=summary" class="parent-image" alt="LOTR picture">
        </div>
        <div id="LEDC" class="small-6 medium-4 large-4 columns">
          <img src="getImage.php?id=2&type=summary" class="parent-image" alt="ledc picture">
        </div>
        <div id="curnican" class="small-6 medium-4 large-4 columns">
          <img src="getImage.php?id=3&type=summary" class="parent-image" alt="curnican picture">
        </div>
        <div id="crown" class="small-6 medium-4 large-4 columns">
          <img src="getImage.php?id=4&type=summary" class="parent-image" alt="crown picture">
        </div>
        <div id="nfl" class="small-6 medium-4 large-4 columns">
          <img src="getImage.php?id=5&type=summary" class="parent-image" alt="nfl picture">
        </div>
        <div id="cakeShop" class="small-6 medium-4 large-4 columns">
          <img src="getImage.php?id=6&type=summary" class="parent-image" alt="cakeShop picture">
        </div>
        <div id="mini" class="small-6 medium-4 large-4 columns">
          <img src="getImage.php?id=7&type=summary" class="parent-image" alt="mini picture">
        </div>
        <div id="teaHouse" class="small-6 medium-4 large-4 columns">
          <img src="getImage.php?id=8&type=summary" class="parent-image" alt="teaHouse picture">
        </div>
        <div id="bank" class="small-6 medium-4 large-4 columns">
          <img src="getImage.php?id=9&type=summary" class="parent-image" alt="bank picture">
        </div>
        <div id="conference" class="small-6 medium-4 large-4 columns">
          <img src="getImage.php?id=10&type=summary" class="parent-image" alt="conference picture">
        </div>
        <div id="lindt" class="small-6 medium-4 large-4 columns">
          <img src="getImage.php?id=11&type=summary" class="parent-image" alt="lindt picture">
        </div>
        <div id="chinaMobile" class="small-6 medium-4 large-4 columns">
          <img src="getImage.php?id=12&type=summary" class="parent-image" alt="chinaMobile picture">
        </div>
      </div>
    </section>
    </div>

    <!-- lightbox -->
    <div class="lightbox">
      <div class="row">
        <i class="fa fa-times close-lightbox"></i>
        <img id="lightboxImage" class="lightbox-img small-12 small-centered medium-6 medium-centered large-6 large-centered columns" src="" alt="">
        <p id="lightboxDesc" class="lightbox-desc"></p>
        <a href="#" id="lightboxLink"></a>
        <div id="lightboxCloseButton"></div>
      </div>
    </div>

    <!-- contact -->
    <section id="contact" class="row">
      <h2 class="hide">Contact</h2>
      <div id="contactTitle" class="small-6 medium-4 large-4 medium-offset-1 large-offset-1 columns">
        <p>Contact</p>
        <hr>
      </div>

        <form id="contactForm" class="small-12 medium-6 large-6 small-centered medium-centered large-centered columns">
        <!--Make sure to give each input a name attribute(name="")-->
           <label for="name">Name: </label>
           <input id="name" name="name" type="text" size="21" maxlength="30" />
           <label for="email" >Email: </label>
           <input id="email" name="email" type="text" size="21" maxlength="30" />
           <label for="message">Message: </label>
           <textarea id="message" name="message"></textarea>
           <input id="submitMessage" name="submit" type="submit" value="Send" />
        </form>

        <div id="sendEmail" class="small-12 medium-8 large-8 medium-offset-2 large-offset-2 columns">
          <p>You can also send me an <br><a href="mailto:jiaqi19860126@gmail.com">EMAIL HERE!</a></p>
        </div>
        <div id="sendMessageResult"></div>   
    </section>


    <footer id="footerBackground">
      <div class="row">
        <div id="icons" class="small-12 medium-12 large-12 small-centered medium-centered large-centered columns">
          <a href="https://www.facebook.com/qi.jia.7334" target="_blank"><img src="images/icons_facebook.svg" width="34" height="34" alt="facebook icon"></a>
           <a href="https://www.instagram.com/real_real_jay/" target="_blank"><img src="images/icons_ins.svg" width="34" height="34" alt="instagram icon"></a>
          <a href="https://www.linkedin.com/in/qi-jia-312b92148/" target="_blank"><img src="images/icons_link.svg" width="34" height="34" alt="linkedin icon"></a>
          <a href="https://www.youtube.com/" target="_blank"><img src="images/icons_youtube.svg" width="34" height="34" alt="youtube icon"></a>
          <a href="https://www.pinterest.com/" target="_blank"><img src="images/icons_pin.svg" width="34" height="34" alt="pinterest icon"></a>
        </div>
      </div>
    </footer>

    <script src="js/vendor/jquery.js"></script>
    <script src="js/vendor/what-input.js"></script>
    <script src="js/vendor/foundation.js"></script>
    <script src="js/main.js"></script>
    <script src="js/app.js"></script>
  </body>
</html>
