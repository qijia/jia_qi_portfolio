<?php
  ini_set("display_errors",1);
  error_reporting(E_ALL);
  include("connect.php");

  //Get the text fields for an image.
  $id = $_GET['id'];
  $fieldsQuery = "SELECT image_description, image_closelinktext, image_websitelinktext, image_sitelink FROM tbl_images WHERE image_id=$id";
  $getFields = mysqli_query($link, $fieldsQuery);

  // Check the result, only get the text fields when there is one and only one record returned.
  if ($getFields->num_rows == 1) {
      // Get the text fields and put them to JSON format.
      $row = $getFields->fetch_assoc();
      $jsonResult = json_encode($row);
      echo $jsonResult;
  } else {
      // Else, just output the error message.
      echo "Cannot find a matching image. Either no row or more than one row found in DB with the specified id.";
  }

  // Close the connection.
  mysqli_close($link);
?>
