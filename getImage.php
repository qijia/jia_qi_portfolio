<?php
  ini_set("display_errors",1);
  error_reporting(E_ALL);
  include("connect.php");

  //Get a single image.
  $id = $_GET['id'];
  $imgType = $_GET['type'];
  $imgQuery = "SELECT * FROM tbl_images WHERE image_id=$id";
  $getImage = mysqli_query($link, $imgQuery);

  // Check the result, only render the image when there is one and only one image record returned.
  if ($getImage->num_rows == 1) {
      // Get the image and render it
      $row = $getImage->fetch_assoc();
      header("Content-type: image/jpg");
      if($imgType=="summary"){
        echo $row['image_summaryimg'];
      }
      else{
        // Render detail image by default.
        echo $row['image_detailimg'];
      }
  } else {
      // Else, just output the error message.
      echo "Cannot find a matching image. Either no row or more than one row found in DB with the specified id.";
  }

  // Close the connection.
  mysqli_close($link);
?>
